export default {
  slides: [
    {
      'id': 0,
      'src': './assets/slides/abstract-architectural-design-architecture-1337285.jpg',
      'alt': 'Abstract architectural',
      'titre': 'Abstract architectural',
      'info': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit et nisi sed blandit. In in nulla rutrum felis elementum ultricies et nec neque. Suspendisse ultrices malesuada sagittis. Praesent tristique enim sed ante finibus vehicula. Morbi eget ante ut lorem iaculis vestibulum. Pellentesque laoreet ut nulla et scelerisque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed a lacinia quam. Aliquam eu lacus blandit, eleifend dolor non, pharetra ex. Praesent in elementum metus. Donec nec mi convallis, tempus augue et, iaculis leo. Suspendisse nibh dui, cursus a congue id, vulputate eget magna.'
    },
    {
      'id': 1,
      'src': './assets/slides/aluminum-architecture-art-1492232.jpg',
      'alt': 'Aluminum architecture art',
      'titre': 'Aluminum architecture art',
      'info': 'Proin eget tortor risus. Curabitur aliquet quam id dui posuere blandit. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur aliquet quam id dui posuere blandit. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 2,
      'src': './assets/slides/apartment-architectural-design-architecture-1878715.jpg',
      'alt': 'Apartment architectural',
      'titre': 'Apartment architectural',
      'info': 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum porta. Pellentesque in ipsum id orci porta dapibus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 3,
      'src': './assets/slides/apartment-architecture-balcony-259950.jpg',
      'alt': 'Apartment architecture',
      'titre': 'Apartment architecture',
      'info': 'Nulla porttitor accumsan tincidunt. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Nulla quis lorem ut libero malesuada feugiat. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 4,
      'src': './assets/slides/architectural-design-architecture-blue-417273.jpg',
      'alt': 'Architectural design blue',
      'titre': 'Architectural design blue',
      'info': 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 5,
      'src': './assets/slides/architectural-design-architecture-buildings-425010.jpg',
      'alt': 'Architectural design buildings',
      'titre': 'Architectural design buildings',
      'info': 'Nulla quis lorem ut libero malesuada feugiat. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec sollicitudin molestie malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 6,
      'src': './assets/slides/architectural-design-architecture-buildings-534174.jpg',
      'alt': 'Architectural design buildings',
      'titre': 'Architectural design buildings',
      'info': 'Pellentesque in ipsum id orci porta dapibus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 7,
      'src': './assets/slides/architectural-design-architecture-buildings-830891.jpg',
      'alt': 'Architectural design buildings',
      'titre': 'Architectural design buildings',
      'info': 'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 8,
      'src': './assets/slides/architecture-art-building-227675.jpg',
      'alt': 'Architecture art building',
      'titre': 'Architecture art building',
      'info': 'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 9,
      'src': './assets/slides/architecture-black-and-white-boardwalk-262367.jpg',
      'alt': 'Black and white boardwalk',
      'titre': 'Black and white boardwalk',
      'info': 'Quisque velit nisi, pretium ut lacinia in, elementum id enim. Pellentesque in ipsum id orci porta dapibus. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    },
    {
      'id': 10,
      'src': './assets/slides/architecture-building-business-258160.jpg',
      'alt': 'Architecture building business',
      'titre': 'Architecture building business',
      'info': 'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Sed porttitor lectus nibh. Donec rutrum congue leo eget malesuada. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh.'
    }
  ]
}
